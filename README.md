# emerald-mgmt
A simple Ruby wrapper for Minecraft servers

__NOTE - This program is no longer maintained, and contains some known design flaws, e.g using GET requests for non-idempotent actions. Please keep this in mind if you use it__

## What's this?

This app is a very, very simple wrapper used to enable remote management for Minecraft services. It implemenents a very minimum amount of features to permit the adminstrator to use the web browser for daily management, instead of a terminal. It is also one of my first 'real' web applications, in a sense :D

It also does starting and stopping automatically in response to configuration and signals, so one can "leave it be" once it has been configured and proven to work.

So far, it doesn't do much beyond exposing a terminal. Backups, for example, you need to take on your own.
## Requirements

You will need a relatively modern version of Ruby (>2.0), and Bundler (for gem management). The author himself has tested this program with Ruby 2.1

## How to install?

As each system is different, and this program does not contain its own installer, it is therefore impossible to give universal guidance to a very precise degree. This should be done:

1. Copy the program to a suitable folder.
2. See _config.rb_, and set all parameters to your liking. _For your security, particularly read the segment about authentication!_
3. Run the server, and/or set it so that it autostarts with your system.
   * Starting is best done with _bundle install_ and _bundle exec ruby ./emerald.rb_, while in the _src_ folder. Autostart depends on your system, but _systemd_ could be a good candidate (it can, for example, send the server a SIGINT signal which signals the server to terminate gracefully).
   * (*nix systems): __Make sure you do not run it as root! This can make any security holes ten times worse!__ You should ensure that Emerald runs as the same user as the server itself, and that user should be restricted solely for its own purpose.

## I found a bug!

### Known flaws

- I need to resolve how to more rigorously prevent HTML styles from polluting the editing box (other than using an actual form box)
- Authentication could be more flexible, more examples on connecting to, say, foreign auth providers

### Something else?
Oh dear! Send in a report (or preferably a pull request), and I'll note it and see what I can do for it. _Be warned though, I do not intend to maintain this program too actively; only enough to suit my own needs!_

## License

Emerald itself is under MIT license, all gems and other components are under their own respective licenses. See _LICENSE_ for further details
