#!/usr/bin/ruby

require 'rubygems'
require 'bundler/setup'
require_relative 'app/web_service'
require_relative 'app/virtual_console'
require_relative 'config'

module Emerald

puts "Emerald #{Emerald::Configuration::VERSION} starting.."

# If we are running it directly, start it as a script
Emerald::WebService.run! if __FILE__==$0
end