require 'sinatra/base'
require_relative '../config'
require_relative 'service_web_console_wrapper'
require 'cgi'

require 'sinatra/json'
require 'json'
require 'date'
require 'htmlentities'
require 'rack/protection'

module Emerald

  # Copyright (c) 2017 arttuys

  # Permission is hereby granted, free of charge, to any person obtaining a copy
  # of this software and associated documentation files (the "Software"), to deal
  # in the Software without restriction, including without limitation the rights
  # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  # copies of the Software, and to permit persons to whom the Software is
  # furnished to do so, subject to the following conditions:

  # The above copyright notice and this permission notice shall be included in all
  # copies or substantial portions of the Software.

  # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  # LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  # SOFTWARE.

  # Stream helper components
  module StreamHelper
    # Registers a stream for the process manager; this helper function is to avoid duplicate code, as streams need special management in case of them getting closed suddenly
    def self.register_process_manager_stream(process_manager, output_stream)
      process_manager.register_output_listener do |line|
        false if output_stream.closed?

        begin
          output_stream << "data: #{line}\n\n"
        rescue
          false
        end

        true
      end
    end
  end

  # Main web service
  class WebService < Sinatra::Base
    # Prepare settings
    set :server, 'thin'
    set :environment, :production
    set :run, true

    # Set the network interface and port
    set :bind, Emerald::Configuration::BIND
    set :port, Emerald::Configuration::WEB_PORT

    # Location of static assets
    set :public_folder, Proc.new { File.join(root, 'static_assets') }

    # Location of templates
    set :views, Proc.new { File.join(root, 'templates') }

    # Prepare the process manager for the Minecraft process. Also define the default stop command
    set :minecraft_process_manager, Emerald::ServiceWebConsoleWrapper.new(Emerald::Configuration::SERVER_COMMAND, Emerald::Configuration::SERVER_PATH, "stop")
    settings.minecraft_process_manager.register_output_listener do |line|
      puts "[Server console]: #{line}"
      true
    end

    # Prepare HTML entities
    set :html_entities, HTMLEntities.new()
    
    # Enforce simple CSRF checks
    use Rack::Protection::RemoteReferrer 
    
    # Define helpers
    helpers do

      # Generates a simple message box, returned as a subcontent unit
      def generate_message_box(main_title, subtitle, message)
        {:title=>main_title, :content=> erb(:message, :locals => {:message_title => subtitle, :message => message})}
      end

      # Forms a page using the main layout and subcontents
      def form_page(title, *content_hashes)
        erb :main_layout, :locals => {:page_title => title, :emerald_version => Emerald::Configuration::VERSION, :subcontent => content_hashes}
      end

      # Generates a status box from a template
      def server_status_box()
        {:title => 'Server status', :content => erb(:status_box)}
      end

      # Generates a server console box
      def form_console_box(terminal_id)
        {:title=> 'Console', :content=> erb(:server_console, :locals => {:terminal_id => terminal_id})}
      end

      # Require authorization for this page or request
      def require_authorization!
        return if authorized?
        headers['WWW-Authenticate'] = 'Basic realm="Please authenticate yourself to access Emerald"'
        halt 401, form_page('401 Not Authorized', generate_message_box('Error', '401 Not Authorized', 'You are not authorized to access this page or resource. Please authenticate yourself.'))
      end

      # Check if our current IP is trusted
      def ip_trusted?
        return true if Emerald::Configuration.is_trusted_ip?(request.ip)
        puts "Foreign (untrusted) IP, demanding username and password: #{request.ip}"
      end

      # Check if we are authorized?
      def authorized?
        # First, check the IP
        return true if ip_trusted?
        # Then, if not OK, verify username and password
        @auth ||= Rack::Auth::Basic::Request.new(request.env)
        return (@auth.provided? && @auth.basic? && @auth.credentials != nil && Emerald::Configuration.is_valid_username_and_password(*@auth.credentials))
      end
    end

    # Main command stream handlers

    post '/streams/minecraft/snd' do
      require_authorization!
      halt 400 unless params[:msg]
      puts "Received raw command: #{params[:msg]}"
      settings.minecraft_process_manager.send_command(settings.html_entities.decode(params[:msg]))
      204 # response without entity body
    end

    get '/streams/minecraft/recv', :provides => 'text/event-stream' do
      require_authorization!
      stream :keep_open do |out|
        # Register a new listener for the client
        Emerald::StreamHelper.register_process_manager_stream(settings.minecraft_process_manager, out)

      end
    end

    # Start the process
    get '/streams/minecraft/start' do
      require_authorization!
      settings.minecraft_process_manager.start_process!
      204
    end

    # Request to stop the process
    get '/streams/minecraft/stop' do
      require_authorization!
      settings.minecraft_process_manager.request_to_terminate("stop")
      204
    end

    # Ask for the status
    get '/server_status_json' do
      json({"is_running" => settings.minecraft_process_manager.active?, "date" => DateTime.now.to_s})
    end

    get '/' do
      require_authorization!
      form_page('Main', server_status_box, form_console_box('minecraft'))
    end

    settings.minecraft_process_manager.start_process! if Emerald::Configuration::AUTOSTART # Automatic start if needed
  end
end
