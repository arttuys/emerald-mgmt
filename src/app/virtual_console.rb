require 'open3'
require 'thread'

module Emerald

  # Copyright (c) 2017 arttuys

  # Permission is hereby granted, free of charge, to any person obtaining a copy
  # of this software and associated documentation files (the "Software"), to deal
  # in the Software without restriction, including without limitation the rights
  # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  # copies of the Software, and to permit persons to whom the Software is
  # furnished to do so, subject to the following conditions:

  # The above copyright notice and this permission notice shall be included in all
  # copies or substantial portions of the Software.

  # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  # LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  # SOFTWARE.

  # A "virtual console" in the sense that you can read lines, and write them
  class VirtualConsole

    # @param start_command [String] The command to start
    # @param working_dir [String] Working directory to change to for the process
    # @param input_handlers [Array<Proc>] An array of procedures that will handle new, sendable input by asking for (arrays of) strings. If a procedure returns a nil, it will be removed from the list
    # @param output_handlers [Array<Proc>] An array of procedures that will handle new output (stdout, stderr). If a procedure returns false, it will be removed from the list
    # @param termination_handlers [Array<Proc>] An array of procedures that will be called when the process terminates.
    def initialize(start_command, working_dir, input_handlers = nil, output_handlers = nil, termination_handlers = nil)
      @start_command = start_command
      @working_dir = working_dir

      raise "Command and working dir must be String!" unless [start_command, working_dir].all? {|obj| obj.is_a?(String)}

      @pid = nil

      # Set up handlers
      @input_handlers = input_handlers || []
      @output_handlers = output_handlers || []

      @termination_handlers = termination_handlers || []

      @last_exit_status = nil
      @process_start_mutex = Mutex.new # Generate a new mutex for process handling
      @handler_mutex = Mutex.new # A mutex to ensure consistency when working with handlers
    end

    # Verifies that the process is actually running and usable (wait thread has not terminated, and the worker thread is also alive)
    # @return [Boolean] If there is a process running
    def is_running?()
      (@worker_thread != nil && @worker_thread.alive?) && (@wait_thread != nil && @worker_thread.alive?)
    end

    # Tries to convert a string to UTF-8 by any means possible
    # As inspired from Stack Overflow: http://stackoverflow.com/questions/16987575/how-can-i-globally-ignore-invalid-byte-sequences-in-utf-8-strings
    # @param str [String] A string to convert
    # @return [String] A string
    def self.to_utf8(str)
      str = str.force_encoding('UTF-8') # Assert it is UTF8
      return str if str.valid_encoding? # If valid, return as is
      str = str.force_encoding('BINARY') # No? Then convert it to a binary string
      return str.encode('UTF-8', invalid: :replace, undef: :replace) # And convert again, forcing it to be valid UTF-8
    end

    # Starts the running of the IO daemon for this process. If it is already running, return false
    # @return [Boolean] False if the process is already running, True otherwise
    def start!()
      begin
      @process_start_mutex.synchronize do
        return false if is_running?
        # Start a new process, with a clean environment
        Bundler.with_clean_env do
          process_stdin, process_stdout_err, @wait_thread = Open3.popen2e(start_command, :chdir=>@working_dir)
          # Update the PID
          @pid = @wait_thread.pid
          # We've made it this far. Fork our process
          @worker_thread = Thread.new do
            begin


              # puts "Started virtual console thread for process #{@pid}"

              # Start executing in a loop
              incoming_buffer = ''
              outgoing_buffer = ''
              while true
                #   puts "IO select"
                readable_streams, writeable_streams, = IO.select([process_stdout_err], [process_stdin])
                # Are we ready to read more?
                if (ready_input = readable_streams[0])
                  #    puts "Reading IO"
                  partial_string = ''
                  ready_input.readpartial(4096, partial_string) # Read as much as we can. This should not block

                  partial_string = Emerald::VirtualConsole.to_utf8(partial_string) # Force to a valid UTF-8 string

                  partial_string.gsub!(/\r\n/, "\n") # Substitute Windows-style newlines with UNIX ones

                  # Add to the buffer
                  incoming_buffer << partial_string

                  #puts "Read partial: #{partial_string}"
                  #puts "Current incoming buffer: #{incoming_buffer}"
                  while incoming_buffer.include?("\n")
                    index = incoming_buffer.index("\n") # Must happen, and must at least be one character
                    substring = index == 0 ? '' : incoming_buffer[0..index-1]
                    alter_handlers do
                      @output_handlers.select! { |handler| handler.call(substring) } # Reject any that do not want more calls
                    end
                    # Apply the remainder
                    incoming_buffer = incoming_buffer[index+1..-1]
                  end
                end
                if (ready_output = writeable_streams[0])
                  #puts "Ready for output"
                  alter_handlers do
                    #puts "Checking for output"
                    @input_handlers.select! do |handler|
                      array = handler.call()
                      #puts "input: #{array.inspect}"
                      if (array == nil)
                        false
                      else
                        true_array = [array].flatten # Accept either a string or an array of strings

                        line_terminator = Gem.win_platform? ? "\r\n" : "\n"

                        unless (true_array.all? {|line| line.empty?})

                        outgoing_buffer << Emerald::VirtualConsole.to_utf8((true_array * line_terminator) + line_terminator) # If on Windows, as \r\n, otherwise in UNIX
                        end

                        true
                      end
                    end
                  end
                  if (!outgoing_buffer.empty?)

                    ready_output.write(outgoing_buffer) # Some strings may not be entirely linear in terms of bytes. To avoid that complexity, we assume that outgoing_buffer should not block for too long once select() has picked it
                      #puts "Wrote to out: '#{outgoing_buffer}'"
                    outgoing_buffer = ''
                  end
                  #puts "Completed cycle"
                end
              end
            rescue SystemCallError, EOFError
              # Most likely an EOF; treat as a silent termination
            rescue Exception => e
              STDERR.puts e.message
              STDERR.puts e.backtrace.inspect
            ensure
              @pid = nil
              [process_stdin, process_stdout_err].each { |io| io.close unless io.closed? }
              @last_exit_status = @wait_thread.value
              @termination_handlers.each { |handler| handler.call }

            end
          end

        end
        return true
      end
      rescue Exception => e
        # An abnormal exception happened outside the handler. Log it to the log
        STDERR.puts(e.message)
        STDERR.puts(e.backtrace.inspect)
      end
    end

    # Wait until the process terminates
    def wait_until_terminates()
      @worker_thread.join
    end

    # Allows access to handler arrays: input handlers, output handlers and termination handlers. It is bound to a mutex to ensure that only one thread operates on the arrays at a time
    def alter_handlers(&b)
      raise 'Block must be set for altering handlers' unless b != nil
      @handler_mutex.synchronize do
        b.call(@input_handlers, @output_handlers, @termination_handlers)
      end
    end

    #@return [String] The command to start upon calling start!()
    attr_accessor :start_command

    #@return [Process::Status] Last exit status
    attr_reader :last_exit_status

    #@return [Integer] Process identifier for the currently running process, NIL otherwise
    attr_reader :pid

  end
end