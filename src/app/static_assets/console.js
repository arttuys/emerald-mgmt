// To avoid global namespace pollution, register most of the data here
var emeraldConsoleUtils;
emeraldConsoleUtils = {

    // JQuery selector for the console
    console_selector_for_id: function(terminal_id) {
        return "#" + terminal_id + "_console"
    },

    // JQuery selector for the editbox
    editbox_selector_for_id: function(terminal_id) {
        return "#" + terminal_id + "_editbox"
    },

    //URL for the receiving id
    recv_url_for_id: function (terminal_id) {
        return "streams/" + terminal_id + "/recv"
    },

    //URL for the sending rows (with parameter: "line")
    snd_url_for_id: function (terminal_id) {
        return "streams/" + terminal_id + "/snd"
    },



    stream_hash: {},

    initialize_for_stream: function (terminal_id) {
        var event_stream = new EventSource(this.recv_url_for_id(terminal_id));

        // Terminal box doesn't need more initialization
        event_stream.onmessage = function (msg) {
            //Decode the JSON
            var json_hash = JSON.parse(msg.data); //JQuery has already decoded it as a JSON object
            if (json_hash == undefined) return;

            // Content
            var message_str = json_hash["line"];
            var is_user_input = json_hash["is_user_input"];

            // Create the node
            var paragraph = document.createElement("p");
            if (is_user_input) paragraph.setAttribute("class", "server"); //Signify that it is a server-related event
            paragraph.appendChild(document.createTextNode(message_str)); //Add the string
            $(emeraldConsoleUtils.console_selector_for_id(terminal_id)).append(paragraph); // And we're done adding

            // Now, autoscroll
            var scroll_height = $(emeraldConsoleUtils.console_selector_for_id(terminal_id))[0].scrollHeight; //Get the DOM element
            var client_height = $(emeraldConsoleUtils.console_selector_for_id(terminal_id))[0].clientHeight; //Client height
            var scroll_top = $(emeraldConsoleUtils.console_selector_for_id(terminal_id)).scrollTop();

            // Calculate the rough scroll scale:
            var scroll_scale = (client_height / Math.max(scroll_height - scroll_top, 0.1));


            if ((scroll_height > client_height) && scroll_scale >= 0.7) {
                // Set the pos
                $(emeraldConsoleUtils.console_selector_for_id(terminal_id)).scrollTop(scroll_height);
            }
        };

        // Activate the edit box
        var editbox_jq_element = $(emeraldConsoleUtils .editbox_selector_for_id(terminal_id));

        editbox_jq_element.attr("contenteditable", true);

        editbox_jq_element.keydown(function(event) {
            var escPressed = event.which == 27, enterPressed = event.which == 13;

            if (escPressed) {
                document.execCommand("undo"); // Reverse latest action
                editbox_jq_element.blur(); // And trigger any registered handlers on this element
            } else if (enterPressed) {
                var textContent = editbox_jq_element.html();
                $.post( emeraldConsoleUtils .snd_url_for_id(terminal_id), { msg: textContent } );
                editbox_jq_element.empty();
                event.preventDefault();
            }
        });

        emeraldConsoleUtils.stream_hash[terminal_id] = event_stream
    }
};

