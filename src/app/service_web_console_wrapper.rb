require_relative 'virtual_console'
require_relative '../config'
require 'thread'
require 'json'
require 'date'

# Copyright (c) 2017 arttuys

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

module Emerald
  # A wrapper that helps working with a virtual console, translating it to the format that a web console understands. Each wrapper is, by definition, a single service
  # that can not be changed, but can be started and stopped at will.
  class ServiceWebConsoleWrapper

    # Initializes a wrapper
    # @param run_command [String] The command to start the service process
    # @param directory [String] If defined, the working directory of the service
    # @param default_stop_command [String] If defined, the default console command to use in case of a request to terminate gracefully
    def initialize(run_command, directory, default_stop_command=nil)
      @virtual_console_process = Emerald::VirtualConsole.new(run_command, directory)

      @run_command = run_command.freeze
      @working_directory = directory.freeze
      @default_stop_command = default_stop_command.freeze

      # Add the handlers
      @virtual_console_process.alter_handlers do |input_handlers, output_handlers, termination_handlers|
        input_handlers << method(:handle_input)
        output_handlers << method(:handle_output)
        termination_handlers << method(:handle_termination)
      end

      @input_output_mutex = Mutex.new # A mutex that locks both input and output so that they must have a definite order
      @process_action_mutex = Mutex.new # A mutex that locks when there are operations pending on a process;

      @line_history = [] # A list of lines that will be output
      @output_listeners = [] # What parties are interested in hearing about the output from the server?
      @input_lines = [] # What lines we have waiting in the queue of commands?

      @default_stop_command = default_stop_command

      # Finally, set up traps
      set_up_traps
    end

    # Parameters
    attr_reader :run_command, :working_directory, :default_stop_command

    # Is this service active?
    # @return [Boolean] True if the process is running, false if not
    def active?()
      @process_action_mutex.synchronize do
        active_no_sync?
      end
    end

    # Sends in a command to the process
    # @param command [String] A row to send to the process
    def send_command(cmd)
      @input_output_mutex.synchronize do
        @input_lines << cmd.to_s
        add_to_history_and_broadcast(generate_line(true, "> #{cmd}")) # Add to line history, and broadcast out command
      end
    end

    # Registers an output listener. Each time there's a line, it receives a JSON with following construction:
    # {is_user_input: true/false,
    #  line: string}, where:
    #
    #  is_user_input: was the line a direct user input; if TRUE, yes, and if FALSE, no (it was from the server process)
    #  text: the text itself
    #
    #  If the listener returns False at any point, it will be removed from the listener queue
    #  @param emit_old_history [Boolean] Should the server send current history to the new client?
    #  @return [Boolean] Returns false if this output listener rejected while history was being sent, true otherwise
    def register_output_listener(emit_old_history=true, &b)
      raise 'Must have a block!' unless block_given?

      @input_output_mutex.synchronize do
        if (emit_old_history)
          @line_history.each do |line|
            return false if b.call(line) == false # Rejected, do not resume
          end
        end

        @output_listeners << b

      end

      return true
    end

    # Start the process and virtual IO
    # @return [Boolean] False if the service was already running, True otherwise
    def start_process!()

      @process_action_mutex.synchronize do
        return false if active_no_sync?
        @virtual_console_process.start!
        @input_output_mutex.synchronize { add_to_history_and_broadcast(generate_line(true, "Launched server on: #{DateTime.now.to_s}, pid: #{@virtual_console_process.pid}")) }
      end

      return true
    end

    # Tries to terminate a process. If specified, a set of commands is run first
    # @param request_command [Array<String>] If specified, command(s) that we should run instead of possible default commands
    # @param wait_to_complete [Boolean] If true, this method will not return until the process has actually been terminated (and barring unusual circumstances, it will due to the usage of KILL signal)
    def request_to_terminate(request_command=nil, wait_to_complete=false)
      actual_command = request_command != nil ? request_command : @default_stop_command

      # First, try to stop the server.
      exiting_thread = Thread.new do
        @process_action_mutex.synchronize do
          return unless active_no_sync?

          termination_happened = false

          @virtual_console_process.alter_handlers do |input, output, termination|
            termination << (lambda do
              termination_happened = true
              false # Remove our handler after the deed is done
            end)
          end

          commands_run = 0
          [actual_command].flatten.select {|x| x.is_a?(String)}.each {|cmd| send_command(cmd); commands_run+=1} # Attempt to run the commands first
          5.times {sleep(5) if (commands_run>0 && !termination_happened)} # Sleep for 30 seconds in 5 second units, if commands were run and termination has not happened

          unless termination_happened
            # Okay, so the timeout expired and termination has not happened. Force the process to quit
            Process::kill("KILL", @virtual_console_process.pid) # Exterminate the process by using a kill signal
          end
        end
      end

      exiting_thread.join if wait_to_complete # If we are to wait to complete, join the thread to ensure it actually runs
    end

    private

    # Ensures that the process we manage has stopped safely
    def ensure_process_stop!()
      return unless active? # If we have stopped, return
      request_to_terminate(nil, true) # Attempt to terminate gracefully; use default command, and require we wait
    end

    # Set up traps for the object to ensure stopping
    def set_up_traps()
      at_exit { ensure_process_stop! }

      [:INT, :TERM].each do |signal|
        old_handler = trap(signal) do
          ensure_process_stop!
          old_handler.call if old_handler.respond_to?(:call)
        end
      end
    end

    # Returns a JSON line for input
    def generate_line(is_user_input, line)
      raise 'IsUserInput must be a bool' unless is_user_input.is_a?(TrueClass) || is_user_input.is_a?(FalseClass)
      hash = {'is_user_input' => is_user_input, 'line' => line}

      return JSON.generate(hash)
    end

    # Adds a line to history, and broadcasts it. NOT MUTEX-SYNCHRONIZED - be careful where you call this!
    # This also automatically trims history when it is too large
    def add_to_history_and_broadcast(line)
      @line_history << line
      @line_history.shift(@line_history.length - Emerald::Configuration::OUTPUT_HISTORY_LENGTH) if @line_history.length > Emerald::Configuration::OUTPUT_HISTORY_LENGTH # Trim if we are too long

      # Call all handlers, and only select those who pass the muster
      @output_listeners.select! { |handler| handler.call(line) != false }
    end

    # Handle input; should not be called manually from elsewhere
    def handle_input()
      @input_output_mutex.synchronize do
        lines = @input_lines.dup
        @input_lines.clear
        lines
      end
    end

    # Handle output; should not be called manually from elsewhere
    def handle_output(line)

      @input_output_mutex.synchronize do
        #puts 'Calling output!'
        add_to_history_and_broadcast(generate_line(false, line)) # This time, it is a line from service!
      end

      true # Ensure we are not removed from the list of handlers
    end

    # Handle eventual process termination; should not be manually called from elsewhere
    def handle_termination()
      @input_output_mutex.synchronize { add_to_history_and_broadcast(generate_line(true, "Server terminated on: #{DateTime.now.to_s}")) }
      true # Ensure we are not removed from the list of handlers
    end

    # Actual processing for active?, simply without the synch guards
    def active_no_sync?()
      @virtual_console_process != nil && @virtual_console_process.is_running?
    end

  end
end