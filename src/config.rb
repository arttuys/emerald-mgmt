require 'ipaddr'
require 'securerandom'
require 'digest/bubblebabble'
# This file contains configuration options for Emerald. Those who are not especially proficient with programming, should only
# edit this file.
module Emerald
  module Configuration

    # What version should be shown as the Emerald version?
    VERSION = '0.2'

    # What should be the working path of the server when the command is ran?
    SERVER_PATH = '/srv/minecraft'

    # What should Emerald run when the server is to be started? Be aware that batch files should be avoided, as they add a layer of indirection; preferably start the server process directly!
    SERVER_COMMAND = 'java -Xms512M -Xmx1G -XX:MaxPermSize=128M -XX:+UseConcMarkSweepGC -jar spigot-1.11.2.jar'

    # By default, bind to the local loopback (no outside connections). Change this to 0.0.0.0 to allow allow connections from elsewhere
    BIND = '127.0.0.1'

    # On what port should the web server run?
    WEB_PORT = 8088

    # Should the server autostart?
    AUTOSTART = true


    # About authentication: Emerald does not distinguish between any user types. You are either authorized, or you are not.
    # You may, by default, authenticate yourself either by connecting from an authorized IP address; or alternatively, if defined,
    # an username-password configuration. Emerald IS NOT designed to be exposed directly to the public Internet, and therefore security
    # has only been designed to prevent opportunists without further reinforcement

    # Here, you may configure which IP address ranges are to be trusted. By default, only local networks (not public Internet!) are allowed
    # to log on without authentication. You may want to look up on how IP addresses are grouped in networks before editing this.
    #
    # @param ip_address [IPAddr] IP address to be used
    # @return [Boolean] True if the user is allowed solely by IP, False otherwise
    def self.is_trusted_ip?(ip_address)
      trusted_networks = ["192.168.0.0/16", "10.0.0.0/8", "172.16.0.0/12", "127.0.0.1"]
      #trusted_networks = [] # Uncomment this, and comment the line above to fully disallow IP-based login

      return trusted_networks.map {|net| IPAddr.new(net)}.any?{|ipaddr_net| ipaddr_net.include?(IPAddr.new(ip_address))} # Allow entry if any of these networks contains the IP address in question
    end

    # Verifies if the given username-password combination is valid.
    # The 'easiest' way to enable username/password-authentication is to hard-code them here; however, this comes with following security implications:
    #   - The valid combination is readable by anyone who has access to this file. You should not use a combination you use anywhere else!
    #   - If your username/password combination is stolen, you need to access this source code to change them. You may want to learn enough Ruby to implement
    #     a simple remote authentication check. You may also consider using a periodically changing secret code, like secret agents do ^^
    #
    # A more optimal solution would be adding a function that checks from a database whenever a pair is valid, and remembers that result for a short period of time.
    # However, that is out of scope for this project
    #
    # @param username [String] Entered username
    # @param password [String] Entered password
    # @return [Boolean] True if a valid combination, False otherwise
    def self.is_valid_username_and_password(username, password)
        #return true if (username == 'Steve' && password == 'diamonds!') # Uncomment this line and modify the credentials to your liking. You may have several of these, if you like

        # As mentioned a few rows before, this has considerable drawbacks
        # A slightly more optimal solution would be using Digest::SHA256.bubblebabble(), as follows:
        #return true if (username == 'Steve' && Digest::SHA256.bubblebabble() == 'xosib-kyryf-buluz-hugep-mynug-gehom-zadum-kubog-redaf-lypob-rotod-tefen-hesir-muboz-nesip-tatog-vuxex')

        # That is considerably harder to guess, don'tcha think?

        return false # Ensures that, if no combination matches, the attempt is not accepted
    end


    # How many lines of history should the server retain at a time?
    OUTPUT_HISTORY_LENGTH = 100

  end
end